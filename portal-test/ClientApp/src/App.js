import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';

/*import cors from 'cors';
import axios from 'axios';
import express from 'express';


const app = express();*/
export default class App extends Component {
    static displayName = App.name;



  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />

      </Layout>
    );
  }
}
